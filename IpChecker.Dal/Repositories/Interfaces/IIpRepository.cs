﻿using IpChecker.Dal.Entities;
using System.Collections.Generic;

namespace IpChecker.Dal.Repositories.Interfaces
{
    /// <summary>
    /// Интерфейс для взаимодействия с таблицой ip адресов
    /// </summary>
    public interface IIpRepository
    {
        /// <summary>
        /// Вставить данные пачкой
        /// </summary>
        /// <param name="ips"></param>
        void BulkInsert(List<IpDTO> ips);

        /// <summary>
        /// Получить адрес по IP
        /// </summary>
        /// <param name="ip">Целочисленное представление ip</param>
        /// <returns>Адрес или код страны</returns>
        AddressDTO GetAddressByIp(uint ip);
    }
}
