﻿using Dapper;
using Dapper.Bulk;
using IpChecker.Dal.Entities;
using IpChecker.Dal.Repositories.Interfaces;
using IpChecker.Dal.Settings;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace IpChecker.Dal.Repositories
{
    public class IpRepository : IIpRepository
    {
        private readonly string _connectionString;
        private readonly ILogger<IpRepository> _logger;

        public IpRepository(IDbSettings dbSettings, ILogger<IpRepository> logger)
        {
            _connectionString = dbSettings.ConnectionString;
            _logger = logger;
        }

        public void BulkInsert(List<IpDTO> ips)
        {
            string currentTime = DateTime.Now.ToString("ddMMyyyy");
            TableMapper.SetupConvention("", currentTime);

            using (var connection = new SqlConnection(_connectionString))
            {
                var ifExistQuery = $"IF OBJECT_ID('ips{currentTime}', 'U') IS NOT NULL SELECT 'true' ELSE SELECT 'false'";
                if (!connection.ExecuteScalar<bool>(ifExistQuery))
                {
                    // запрос формует структуру таблицы, template таблица всегда пустая
                    var createTable = $@"SELECT *
                                    INTO    [dbo].[ips{currentTime}]
                                    FROM    [dbo].[ipsTemplate]";
                    connection.Execute(createTable);

                    _logger.LogInformation("Created today's table");
                }
                else
                {
                    _logger.LogInformation("Today's data is exist. Stopped inserting data.");
                    return;
                }
            }

            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlTransaction trans = connection.BeginTransaction())
                {
                    try
                    {
                        connection.Execute($@"
                        insert ips{currentTime}(IpMax, IpMin, IpMaxBytes, IpMinBytes, CountryCode, Address, Latitude, Longitude)
                        values(@IpMax, @IpMin, @IpMaxBytes, @IpMinBytes, @CountryCode, @Address, @Latitude, @Longitude)", ips, transaction: trans);
                        trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }

                _logger.LogInformation("Inserted data to today's table");
            }

            using (var connection = new SqlConnection(_connectionString))
            {
                // создаем или перезаписываем view на новую таблицу с новыми данными
                var queryCreateOrUpdate = $@"IF OBJECT_ID('dbo.ips') IS NULL
                                            BEGIN
                                                exec('CREATE VIEW ips
                                                AS
                                                SELECT * FROM ips{currentTime}')
                                            END
                                            ELSE
                                                exec('ALTER VIEW dbo.ips
                                                AS
                                                SELECT * FROM ips{currentTime}')";
                connection.Execute(queryCreateOrUpdate);

                _logger.LogInformation("View has created or updated which mapped to today's table");
            }

            using (var connection = new SqlConnection(_connectionString))
            {

                // удаляем старую таблицу
                var yesterdayDateTime = DateTime.Now.AddDays(-1).ToString("ddMMyyyy");
                var dropOldTableQuery = $"DROP TABLE IF EXISTS [dbo].ips{yesterdayDateTime}";
                connection.Execute(dropOldTableQuery);

                _logger.LogInformation("Old table has dropped");
            }
            // есть вероятность появления старых таблиц, при перерывах в работе планировщика
            // можно решить добавлением 2 таблиц между которыми отдельно переключать view

        }

        public AddressDTO GetAddressByIp(uint ip)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                var query = $"SELECT Address, CountryCode FROM Ips WHERE CAST({ip} as bigint) <= IpMaxBytes AND CAST({ip} as bigint) >= IpMinBytes";
                return connection.QuerySingleOrDefault<AddressDTO>(query);
            }
        }
    }
}
