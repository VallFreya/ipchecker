﻿namespace IpChecker.Dal.Settings
{
    public interface IDbSettings
    {
        /// <summary>
        /// Строка подключения к базе данных
        /// </summary>
        string ConnectionString { get; set; }
    }
}
