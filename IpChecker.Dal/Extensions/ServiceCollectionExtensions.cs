﻿using IpChecker.Dal.Repositories;
using IpChecker.Dal.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace IpChecker.Dal.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDal(this IServiceCollection services)
        {
            services.AddTransient<IIpRepository, IpRepository>();
            return services;
        }
    }
}
