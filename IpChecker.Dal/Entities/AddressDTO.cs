﻿namespace IpChecker.Dal.Entities
{
    public class AddressDTO
    {
        /// <summary>
        /// Код страны
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }
    }
}
