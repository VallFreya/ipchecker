﻿using System.ComponentModel.DataAnnotations.Schema;

namespace IpChecker.Dal.Entities
{
    [Table("Ips")]
    public class IpDTO
    {
        /// <summary>
        /// Максимальное начение IP
        /// </summary>
        public string IpMax { get; set; }

        /// <summary>
        /// Минимальное значение IP
        /// </summary>
        public string IpMin { get; set; }

        /// <summary>
        /// Максимальное значение Ip в байтах
        /// </summary>
        public string IpMaxBytes { get; set; }

        /// <summary>
        /// Минимальное значение Ip в байтах
        /// </summary>
        public string IpMinBytes { get; set; }

        /// <summary>
        /// Двухзначный код страны
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public string Longitude { get; set; }
    }
}
