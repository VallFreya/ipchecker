SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[IpsTemplate](
	[IpMax] [nchar](20) NOT NULL,
	[IpMin] [nchar](20) NOT NULL,
	[CountryCode] [nchar](4) NOT NULL,
	[IpMaxBytes] [bigint] NOT NULL,
	[IpMinBytes] [bigint] NOT NULL,
	[Address] [nvarchar](200) NULL,
	[Latitude] [nvarchar](50) NULL,
	[Longitude] [nvarchar](50) NULL
) ON [PRIMARY]
GO