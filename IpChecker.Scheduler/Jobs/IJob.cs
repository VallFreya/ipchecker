﻿namespace IpChecker.Scheduler.Jobs
{
    /// <summary>
    /// Интерфейс, описывающий задачу
    /// </summary>
    public interface IJob
    {
        /// <summary>
        /// Крон выражение периодичности запуска задачи
        /// </summary>
        string GetCron();

        /// <summary>
        /// Начать задачу
        /// </summary>
        void Start(); 
    }
}
