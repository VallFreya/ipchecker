﻿using Hangfire;
using IpChecker.Dal.Entities;
using IpChecker.Dal.Repositories.Interfaces;
using IpChecker.Scheduler.Parsers.Interfaces;
using IpChecker.Scheduler.Services.Interfaces;
using IpChecker.Scheduler.Settings;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IpChecker.Scheduler.Jobs
{
    [DisableConcurrentExecution(timeoutInSeconds: 10 * 60)]
    public class UpdateGeoInfoJob : IJob
    {
        private readonly AppSettings _appSettings;
        private readonly IDownloadService _downloadService;
        private readonly IFileService _fileService;
        private readonly ICityParser _cityParser;
        private readonly IIpParser _ipParser;
        private readonly IIpRepository _ipRepository;
        private readonly ILogger<UpdateGeoInfoJob> _logger;

        /// <summary>
        /// Каждые 5 минут
        /// </summary>
        /// <returns></returns>
        public string GetCron() => "*/5 * * * *";

        public UpdateGeoInfoJob(
            AppSettings appSettings, 
            IDownloadService downloadService, 
            IFileService fileService, 
            ICityParser cityParser, 
            IIpParser ipParser, 
            IIpRepository ipRepository,
            ILogger<UpdateGeoInfoJob> logger)
        {
            _appSettings = appSettings;
            _downloadService = downloadService;
            _fileService = fileService;
            _cityParser = cityParser;
            _ipParser = ipParser;
            _ipRepository = ipRepository;
            _logger = logger;
        }

        public void Start()
        {
            _logger.LogInformation("Start import ip data from web");
            try
            {
                var IpGeoFileFullPath = Environment.CurrentDirectory + _appSettings.IpGeoFileLocalFolderPath;
                var fileName = _downloadService.DownloadFile(_appSettings.IpGeoFileArchiveUrl, _appSettings.IpGeoFileLocalFolderPath);
                _logger.LogInformation("File has downloaded");

                var files = _fileService.UnpackRar(IpGeoFileFullPath + fileName, IpGeoFileFullPath);
                _logger.LogInformation("Archive has unpacked");

                if (files is null || files.Count == 0)
                {
                    _logger.LogError("There isn't files in achive. Parsing has stoped.");
                    return;
                }

                var cities = _cityParser.Parse(IpGeoFileFullPath + files.Where(x => x.Contains("cities.txt")).FirstOrDefault());
                var ips = _ipParser.Parse(IpGeoFileFullPath + files.Where(x => x.Contains("cidr_optim.txt")).FirstOrDefault());

                _logger.LogInformation("Files have parsed");

                if (ips is null || ips.Count == 0) 
                {
                    _logger.LogError("There isn't ips. Program has stoped.");
                    return;
                }

                List<IpDTO> entities = ips.Select(x =>
                {
                    var city = cities.Where(y => y.Id == x.CityId).FirstOrDefault();
                    var dto = new IpDTO
                    {
                        IpMax = x.IpMax,
                        IpMin = x.IpMin,
                        IpMaxBytes = x.IpMaxBytes,
                        IpMinBytes = x.IpMinBytes,
                        CountryCode = x.CountryCode,
                        Address = city?.Address,
                        Latitude = city?.Latitude,
                        Longitude = city?.Longitude
                    };

                    return dto;
                }).ToList();

                _logger.LogInformation("Data have mapped");

                _ipRepository.BulkInsert(entities);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Stopped program because of exception");
                throw;
            }

            _logger.LogInformation("Job has finished");
        }
    }
}
