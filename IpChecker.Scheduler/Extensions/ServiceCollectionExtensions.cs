﻿using IpChecker.Dal.Extensions;
using IpChecker.Scheduler.Jobs;
using IpChecker.Scheduler.Parsers;
using IpChecker.Scheduler.Parsers.Interfaces;
using IpChecker.Scheduler.Services;
using IpChecker.Scheduler.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace IpChecker.Scheduler.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static ServiceCollection AddSchedulerServices(this ServiceCollection services) 
        {
            services.AddTransient<IDownloadService, DownloadService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<ICityParser, CityParser>();
            services.AddTransient<IIpParser, IpParser>();
            services.AddDal();
            return services;
        }

        public static ServiceCollection AddJobs(this ServiceCollection services)
        {
            services.AddTransient<IJob, UpdateGeoInfoJob>();
            services.AddTransient<UpdateGeoInfoJob, UpdateGeoInfoJob>();
            return services;
        }
    }
}
