﻿namespace IpChecker.Scheduler.Entities
{
    public class City
    {
        /// <summary>
        /// Целочисленный идентификатор города
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Широта
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Долгота
        /// </summary>
        public string Longitude { get; set; }

        
    }
}
