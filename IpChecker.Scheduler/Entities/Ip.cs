﻿namespace IpChecker.Scheduler.Entities
{
    public class Ip
    {
        /// <summary>
        /// Максимальное начение IP
        /// </summary>
        public string IpMax { get; set; }

        /// <summary>
        /// Минимальное значение IP
        /// </summary>
        public string IpMin { get; set; }

        /// <summary>
        /// Максимальное значение Ip в байтах
        /// </summary>
        public string IpMaxBytes { get; set; }

        /// <summary>
        /// Минимальное значение Ip в байтах
        /// </summary>
        public string IpMinBytes { get; set; }

        /// <summary>
        /// Двухзначный код страны
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Id города, может быть пустым
        /// </summary>
        public string CityId { get; set; }
    }
}
