﻿using IpChecker.Dal.Settings;

namespace IpChecker.Scheduler.Settings
{
    /// <summary>
    /// Конфигурация приложения 
    /// </summary>
    public class AppSettings : IDbSettings
    {
        /// <summary>
        /// Строка подключения к основной базе данных
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Строка подключения к базе данных Hangfire
        /// </summary>
        public string HangfireConnectionString { get; set; }

        /// <summary>
        /// Ссылка для скачивания архива
        /// </summary>
        public string IpGeoFileArchiveUrl { get; set; }

        /// <summary>
        /// Папка для архива
        /// </summary>
        public string IpGeoFileLocalFolderPath { get; set; }
    }
}
