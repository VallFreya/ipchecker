﻿using IpChecker.Scheduler.Entities;
using IpChecker.Scheduler.Parsers.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IpChecker.Scheduler.Parsers
{
    public class IpParser : IIpParser
    {
        private readonly ILogger<IpParser> _logger;

        public IpParser(ILogger<IpParser> logger)
        {
            _logger = logger;
        }
        public List<Ip> Parse(string filepath)
        {
            var ips = new List<Ip>();
            try
            {

                using StreamReader sr = new StreamReader(filepath, Encoding.GetEncoding(1251));
                while (sr.Peek() >= 0)
                {
                    var data = sr.ReadLine().Split('\t');
                    ips.Add(new Ip
                    {
                        IpMinBytes = data[0].Trim(),
                        IpMaxBytes = data[1].Trim(),
                        IpMin = data[2].Split('-')[0].Trim(),
                        IpMax = data[2].Split('-')[1].Trim(),
                        CountryCode = data[3].Trim(),
                        CityId = data[4] == "-" ? string.Empty : data[4].Trim()
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while ips file");
            }

            return ips;
        }
    }
}
