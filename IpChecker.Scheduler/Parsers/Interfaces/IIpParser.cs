﻿using IpChecker.Scheduler.Entities;
using System.Collections.Generic;

namespace IpChecker.Scheduler.Parsers.Interfaces
{
    public interface IIpParser
    {
        /// <summary>
        /// Распарсить список Ip
        /// </summary>
        /// <param name="filepath">Путь к файлу</param>
        /// <returns>Список Ip</returns>
        List<Ip> Parse(string filepath);
    }
}
