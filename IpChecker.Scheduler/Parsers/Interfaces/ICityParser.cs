﻿using IpChecker.Scheduler.Entities;
using System.Collections.Generic;

namespace IpChecker.Scheduler.Parsers.Interfaces
{
    public interface ICityParser
    {
        /// <summary>
        /// Распарсить список городов
        /// </summary>
        /// <param name="filepath">Путь к файлу с городами</param>
        /// <returns>Список городов</returns>
        List<City> Parse(string filepath);
    }
}
