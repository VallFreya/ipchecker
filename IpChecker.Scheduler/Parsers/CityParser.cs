﻿using IpChecker.Scheduler.Entities;
using IpChecker.Scheduler.Parsers.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IpChecker.Scheduler.Parsers
{
    public class CityParser : ICityParser
    {
        private readonly ILogger<CityParser> _logger;

        public CityParser(ILogger<CityParser> logger)
        {
            _logger = logger;
        }

        public List<City> Parse(string filepath)
        {
            var cities = new List<City>();
            try
            {

                using StreamReader sr = new StreamReader(filepath, Encoding.GetEncoding(1251));
                while (sr.Peek() >= 0)
                {
                    var data = sr.ReadLine().Split('\t');
                    cities.Add(new City
                    {
                        Id = data[0],
                        Address = data[1] + ", " + data[2] + ", " + data[3],
                        Latitude = data[4],
                        Longitude = data[5]
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while parsing cities file.");
            }

            return cities;
        }
    }
}
