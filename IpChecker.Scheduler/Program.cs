﻿using IpChecker.Scheduler.Extensions;
using IpChecker.Scheduler.Jobs;
using IpChecker.Scheduler.Settings;
using Hangfire;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Hangfire.MemoryStorage;
using System.Text;
using IpChecker.Dal.Settings;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog;
using System.IO;

namespace IpChecker.Scheduler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = LogManager.GetCurrentClassLogger();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var config = builder.Build();
            var appSettings = new AppSettings();
            config.GetSection(nameof(AppSettings)).Bind(appSettings);
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSchedulerServices();
            serviceCollection.AddJobs();
            serviceCollection.AddTransient<AppSettings>((provider) => appSettings);
            serviceCollection.AddTransient<IDbSettings>((provider) => appSettings);
            serviceCollection.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                loggingBuilder.AddNLog();
            });
            var serviceProvider = serviceCollection.BuildServiceProvider();

            GlobalConfiguration.Configuration
                //.UseMemoryStorage()
                .UseSqlServerStorage($"{appSettings.HangfireConnectionString}")
                .UseColouredConsoleLogProvider()
                .UseActivator(new HangfireActivator(serviceProvider));

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            try
            {
                var jobs = serviceProvider.GetServices<IJob>();
                foreach (var job in jobs)
                {
                    RecurringJob.AddOrUpdate(nameof(job), () => job.Start(), job.GetCron());
                }

                using var server = new BackgroundJobServer();
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Stopped program because of exception");
                throw;
            }
            finally
            {
                LogManager.Shutdown();
            }
        }
    }
}
