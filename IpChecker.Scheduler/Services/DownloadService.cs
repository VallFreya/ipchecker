﻿using IpChecker.Scheduler.Services.Interfaces;
using System;
using System.IO;
using System.Net.Http;

namespace IpChecker.Scheduler.Services
{
    public class DownloadService : IDownloadService
    {
        public string DownloadFile(string url, string localFolder)
        {
            var fileName = "file.rar";
            var forder = Environment.CurrentDirectory + localFolder;
            var file = forder + fileName;
            if (!Directory.Exists(forder))
            {
                Directory.CreateDirectory(forder);
            }

            if (File.Exists(file))
            {
                File.Delete(file);
            }

            var uri = new Uri(url);
            HttpClient client = new HttpClient();
            var response = client.GetAsync(uri)
                .ConfigureAwait(false).GetAwaiter().GetResult();
            using var fs = new FileStream(Path.GetFullPath(Environment.CurrentDirectory + localFolder + "/" + fileName), FileMode.CreateNew);
            response.Content.CopyToAsync(fs)
                .ConfigureAwait(false).GetAwaiter().GetResult();
            return fileName;
        }
    }
}
