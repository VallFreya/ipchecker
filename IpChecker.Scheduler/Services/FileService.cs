﻿using IpChecker.Scheduler.Services.Interfaces;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace IpChecker.Scheduler.Services
{
    public class FileService : IFileService
    {
        public List<string> UnpackRar(string filepath, string to)
        {
            var filesNames = new List<string>();
            using (ZipArchive zip = ZipFile.OpenRead(filepath))
            {
                foreach (var file in zip.Entries)
                {
                    filesNames.Add(file.Name);
                    if (File.Exists(to + file.Name))
                    {
                        File.Delete(to + file.Name); 
                    }
                }
            }

            ZipFile.ExtractToDirectory(filepath, to);
            return filesNames;
        }
    }
}
