﻿namespace IpChecker.Scheduler.Services.Interfaces
{
    public interface IDownloadService
    {
        /// <summary>
        /// Скачать файл
        /// </summary>
        /// <param name="url">Ссылка на скачивание файла</param>
        /// <param name="localFolder">Папка для хранения файла</param>
        /// <returns>Наименование файла</returns>
        string DownloadFile(string url, string localFolder);
    }
}
