﻿using System.Collections.Generic;

namespace IpChecker.Scheduler.Services.Interfaces
{
    public interface IFileService
    {

        /// <summary>
        /// Распаковать файл
        /// </summary>
        /// <param name="filepath">Путь к архиву</param>
        /// <param name="to">Путь к папке, куда будет распакован архив</param>
        /// <returns>Список файлов внутри архива</returns>
        List<string> UnpackRar(string filepath, string to);
    }
}
