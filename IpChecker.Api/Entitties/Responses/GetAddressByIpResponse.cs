﻿namespace IpChecker.Api.Entitties.Responses
{
    public class GetAddressByIpResponse
    {
        public string CountryCode { get; set; }

        public string Address { get; set; }
    }
}
