﻿using IpChecker.Dal.Settings;

namespace IpChecker.Api.Settings
{
    public class AppSettings : IDbSettings
    {
        /// <summary>
        /// Строка подключения к базе данных
        /// </summary>
        public string ConnectionString { get; set; }
    }
}
