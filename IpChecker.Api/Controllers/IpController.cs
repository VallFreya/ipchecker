﻿using IpChecker.Api.Entitties.Responses;
using IpChecker.Services.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace IpChecker.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IpController : ControllerBase
    {
        private readonly IIpService _ipService;
        private readonly ILogger<IpController> _logger;

        public IpController(IIpService ipService, ILogger<IpController> logger)
        {
            _ipService = ipService;
            _logger = logger;
        }

        [HttpGet("address/{ip}")]
        public ActionResult<GetAddressByIpResponse> GetAddressByIp([FromRoute]string ip)
        {
            try
            {
                var address = _ipService.GetAddressByIp(ip);
                return new GetAddressByIpResponse
                {
                    Address = address.AddressValue,
                    CountryCode = address.CountryCode
                };
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception while getting ip.");
                return NotFound();
            }

        }
    }
}