﻿using IpChecker.Api.Controllers;
using IpChecker.Services.Entities;
using IpChecker.Services.Services.Interfaces;
using IpChecker.Tests.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace IpChecker.Tests.Api.Controllers
{
    public class IpControllerTests : BaseAutoMock<IpController>
    {
        [Fact]
        public void GetAddress_NotFound_Test()
        {
            Sut.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            mock.Mock<IIpService>();
            var result = Sut.GetAddressByIp("some ip");
            Assert.IsAssignableFrom<NotFoundResult>(result.Result);
        }

        [Fact]
        public void GetAddress_Return_Correct_Test()
        {
            var address = "some address";
            Sut.ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext() };
            var ipService = mock.Mock<IIpService>();
            ipService.Setup(x => x.GetAddressByIp(It.IsAny<string>())).Returns(() => new Address { AddressValue = address, CountryCode = "ru" });
            var result = Sut.GetAddressByIp("some ip");
            Assert.True(result.Value.Address == address);
        }
    }
}
