﻿using IpChecker.Dal.Repositories.Interfaces;
using IpChecker.Services.Services;
using IpChecker.Tests.Common;
using Moq;
using Xunit;

namespace IpChecker.Tests.Services
{
    public class IpServiceTests : BaseAutoMock<IpService>
    {
        [Theory]
        [InlineData("2.16.7.255", 34605055)]
        [InlineData("2.92.59.255", 39599103)]
        public void GetAddressByIp_CorrectConvertIp_Test(string ip, uint uintIp)
        {
            var repository = mock.Mock<IIpRepository>();
            
            Sut.GetAddressByIp(ip);
            repository.Verify(s => s.GetAddressByIp(It.Is<uint>(ipValue => ipValue == uintIp)), Times.Once);
        }
    }
}
