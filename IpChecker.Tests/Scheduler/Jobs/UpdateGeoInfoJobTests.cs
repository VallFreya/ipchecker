﻿using IpChecker.Dal.Entities;
using IpChecker.Dal.Repositories.Interfaces;
using IpChecker.Scheduler.Entities;
using IpChecker.Scheduler.Jobs;
using IpChecker.Scheduler.Parsers.Interfaces;
using IpChecker.Scheduler.Services.Interfaces;
using IpChecker.Tests.Common;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace IpChecker.Tests.Scheduler.Jobs
{
    public class UpdateGeoInfoJobTests : BaseAutoMock<UpdateGeoInfoJob>
    {
        [Fact]
        public void Job_Archive_Is_Empty_Test()
        {
            var downloadService = mock.Mock<IDownloadService>();
            var fileService = mock.Mock<IFileService>();
            var cityParser = mock.Mock<ICityParser>();
            var ipParser = mock.Mock<IIpParser>();
            var ipRepository = mock.Mock<IIpRepository>();

            Sut.Start();

            downloadService.Verify(x => x.DownloadFile(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            fileService.Verify(x => x.UnpackRar(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            cityParser.Verify(x => x.Parse(It.IsAny<string>()), Times.Never);
            ipParser.Verify(x => x.Parse(It.IsAny<string>()), Times.Never);
            ipRepository.Verify(x => x.BulkInsert(It.IsAny<List<IpDTO>>()), Times.Never);
        }

        [Fact]
        public void Job_Archive_Has_Files_Test()
        {
            var downloadService = mock.Mock<IDownloadService>();
            var fileService = mock.Mock<IFileService>();
            var cityParser = mock.Mock<ICityParser>();
            var ipParser = mock.Mock<IIpParser>();
            var ipRepository = mock.Mock<IIpRepository>();
            fileService.Setup(x => x.UnpackRar(It.IsAny<string>(), It.IsAny<string>())).Returns(() => new List<string> { "cities.txt", "cidr_optim.txt" });

            Sut.Start();

            downloadService.Verify(x => x.DownloadFile(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            fileService.Verify(x => x.UnpackRar(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            cityParser.Verify(x => x.Parse(It.IsAny<string>()), Times.Once);
            ipParser.Verify(x => x.Parse(It.IsAny<string>()), Times.Once);

            ipRepository.Verify(x => x.BulkInsert(It.IsAny<List<IpDTO>>()), Times.Never);
        }

        [Fact]
        public void Job_Mapped_Data_Test()
        {
            var address = "some address";
            var downloadService = mock.Mock<IDownloadService>();
            var fileService = mock.Mock<IFileService>();
            var cityParser = mock.Mock<ICityParser>();
            var ipParser = mock.Mock<IIpParser>();
            var ipRepository = mock.Mock<IIpRepository>();
            fileService.Setup(x => x.UnpackRar(It.IsAny<string>(), It.IsAny<string>())).Returns(() => new List<string> { "cities.txt", "cidr_optim.txt" });
            cityParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(() => new List<City> { new City {
                Id = "1",
                Address = address,
                Latitude = "1.11",
                Longitude = "1. 11"
            } });
            ipParser.Setup(x => x.Parse(It.IsAny<string>())).Returns(() => new List<Ip> { new Ip { 
                CityId = "1",
                CountryCode = "ru",
                IpMax = "127.0.0.10",
                IpMin = "127.0.0.1",
                IpMaxBytes = "123",
                IpMinBytes = "124"
            } });
            Sut.Start();

            downloadService.Verify(x => x.DownloadFile(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            fileService.Verify(x => x.UnpackRar(It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            cityParser.Verify(x => x.Parse(It.IsAny<string>()), Times.Once);
            ipParser.Verify(x => x.Parse(It.IsAny<string>()), Times.Once);

            ipRepository.Verify(x => x.BulkInsert(It.Is<List<IpDTO>>(x => x.First().Address == address)), Times.Once);
        }

        [Fact]
        public void Job_Catch_Exception_Test()
        {
            var downloadService = mock.Mock<IDownloadService>();
            downloadService.Setup(x => x.DownloadFile(It.IsAny<string>(), It.IsAny<string>())).Returns(() => throw new Exception());

            Assert.Throws<Exception>(() => Sut.Start());

            downloadService.Verify(x => x.DownloadFile(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
    }
}
