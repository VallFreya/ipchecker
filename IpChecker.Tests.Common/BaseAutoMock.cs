﻿using Autofac.Extras.Moq;
using System;

namespace IpChecker.Tests.Common
{
    /// <summary>
    /// Класс для автомоков тестов
    /// </summary>
    /// <typeparam name="T">Тестируемый класс</typeparam>
    public class BaseAutoMock<T> : IDisposable
    {
        protected AutoMock mock;

        private readonly Lazy<T> _lazySut;

        protected BaseAutoMock()
        {
            mock = AutoMock.GetLoose();
            _lazySut = new Lazy<T>(() => mock.Create<T>());
        }

        /// <summary>
        /// Subject of Unit Testing
        /// </summary>
        protected T Sut => _lazySut.Value;

        public void Dispose()
        {
            mock?.Dispose();
        }
    }

}
