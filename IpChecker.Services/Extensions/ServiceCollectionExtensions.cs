﻿using IpChecker.Dal.Extensions;
using IpChecker.Services.Services;
using IpChecker.Services.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace IpChecker.Services.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IIpService, IpService>();
            services.AddDal();
            return services;
        }
    }
}
