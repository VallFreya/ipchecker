﻿using IpChecker.Dal.Repositories.Interfaces;
using IpChecker.Services.Entities;
using IpChecker.Services.Services.Interfaces;
using System;
using System.Linq;
using System.Net;

namespace IpChecker.Services.Services
{
    public class IpService : IIpService
    {
        private readonly IIpRepository _ipRepository;

        public IpService(IIpRepository ipRepository)
        {
            _ipRepository = ipRepository;
        }

        public Address GetAddressByIp(string ip)
        {
            IPAddress ipAddress = IPAddress.Parse(ip);
            var ipBytes = BitConverter.ToUInt32(ipAddress.GetAddressBytes().Reverse().ToArray(), 0);
            var entity = _ipRepository.GetAddressByIp(ipBytes);
            return new Address
            {
                AddressValue = entity?.Address,
                CountryCode = entity?.CountryCode
            };
        }
    }
}
