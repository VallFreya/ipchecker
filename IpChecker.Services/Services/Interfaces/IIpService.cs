﻿using IpChecker.Services.Entities;

namespace IpChecker.Services.Services.Interfaces
{
    /// <summary>
    /// Интерфейс для работы с адресами
    /// </summary>
    public interface IIpService
    {
        /// <summary>
        /// Получить адрес по ip
        /// </summary>
        /// <param name="ip">Ip адрес Example: 127.0.0.1</param>
        /// <returns></returns>
        Address GetAddressByIp(string ip);
    }
}
