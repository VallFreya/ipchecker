﻿namespace IpChecker.Services.Entities
{
    public class Address
    {
        /// <summary>
        /// Код страны
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Адрес
        /// </summary>
        public string AddressValue { get; set; }
    }
}
